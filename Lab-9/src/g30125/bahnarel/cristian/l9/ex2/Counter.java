package g30125.bahnarel.cristian.l9.ex2;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Counter extends JFrame {

    private JPanel contentPane;
    private JTextField DisplayOne;

    int count;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run()
            {
                try {
                    Counter frame = new Counter();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Counter()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 100, 200, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);

        DisplayOne = new JTextField();
        panel.add(DisplayOne);
        DisplayOne.setColumns(10);

        JButton btnCount = new JButton("Click me");
        contentPane.setBounds(10,24,200,200);

        count=1;
        btnCount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0)
            {
                DisplayOne.setText(Integer.toString(count++));
            }
        });
        panel.add(btnCount);
    }

}