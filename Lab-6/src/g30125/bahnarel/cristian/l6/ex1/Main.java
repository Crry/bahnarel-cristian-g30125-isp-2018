package ex1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.ORANGE, 175,175,"Cercul_1",50,true);
        b1.addShape(s1);
        b1.deleteShape(s1);
        Shape s2 = new Circle(Color.GREEN, 150,150,"Cercul_2",100,false);
        b1.addShape(s2);
        b1.deleteShape(s2);
        //ex1.Shape[].setID().equals("Cercul_1");
        //DrawingBoard b2 = new DrawingBoard();
        Shape s3 = new Rectangle(Color.BLUE, 75,75,"Patratul_1",75,true);
        b1.addShape(s3);
        b1.deleteShape(s3);
        }
}