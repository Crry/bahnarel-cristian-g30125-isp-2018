package ex1;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];
    //ArrayList<ex1.Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(500,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void deleteShape(Shape s1){
        for (int i=0;i<shapes.length;i++){
            if(shapes[i]!=null){
                if (shapes[i].getId().equals("Cercul_2")) {
                    shapes[i] = null;
                    break;
                }
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(ex1.Shape s:shapes)
//            s.draw(g);
    }
}