package ex1;

class Cofee{
    private int temp;
    private int conc;
    private int cmak;

    Cofee(int t,int c, int p){temp = t;conc = c; cmak = p;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getCofMak(){return cmak;}
    public String toString(){return "[cofee number="+cmak+ "; temperature="+temp+"; concentration="+conc+"]";}
}