package g30125.bahnarel.cristian.l2.e4;

import java.util.ArrayList;
import java.util.Scanner;

public class e4 {
    public static void main(String[] args)
    {
        Scanner numar = new Scanner(System.in);
        System.out.print("n=");
        int n = numar.nextInt();
        int[] vector;
        vector = new int[n];
        //ArrayList<Integer> vector = new ArrayList<Integer>();
        for (int i = 0; i< n ; i++)
        {
            System.out.print("vector[" + i + "]=");
            vector[i] = numar.nextInt();
        }
        int max=vector[0];
        for (int j = 1; j<n ; j++)
        {
            if (vector[j] > max)
            {
                max = vector[j];
            }
        }
        System.out.println("Maximul dintre elementele vectorului este: " + max);
    }
}
