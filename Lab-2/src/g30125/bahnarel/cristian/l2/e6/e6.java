package g30125.bahnarel.cristian.l2.e6;

import java.util.Random;
import java.util.Scanner;

public class e6 {
    /*public static void main(String[] args)
    {
        Scanner numar = new Scanner(System.in);
        int n = numar.nextInt();
        int factorialul=1;
        if (n>=0)
        {
            for (int i = 1; i <= n; i++)                                      // subpunctul a
            {
                factorialul *= i;
            }
            System.out.println("Factorialul numarului " + n + " este: " + factorialul);
        }
        else
            {
                System.out.println("Introduceti un numar nenegativ.");
            }
    }*/
        static int fct_fact(int nr)
        {
            if (nr==0) return 1;
            else
            {
                return nr*fct_fact(nr-1);
            }
        }
    public static void main(String[] args)
    {

        Scanner numar = new Scanner(System.in);
                int n = numar.nextInt();                                      //subpunctul b
                int m = fct_fact(n);
        System.out.println("Factorialul numarului " + n + " este: " + m);

    }
}
