import becker.robots.*;

public class ex3 {
    public static void main(String[] args)
    {
        City cluj = new City();
        Robot r1 = new Robot(cluj, 6, 2, Direction.NORTH);
        r1.move();
        r1.move();
        r1.move();
        r1.move();
        r1.move();
        r1.turnLeft();
        r1.turnLeft();
        r1.move();
        r1.move();
        r1.move();
        r1.move();
        r1.move();
    }
}
