import becker.robots.*;

public class ex4 {
    public static void main(String[] args)
    {
        City Cluj=new City();
        Wall block1=new Wall(Cluj,1,1,Direction.WEST);
        Wall block2=new Wall(Cluj,2,1,Direction.WEST);
        Wall block3=new Wall(Cluj,2,1,Direction.SOUTH);
        Wall block4=new Wall(Cluj,2,2,Direction.SOUTH);
        Wall block5=new Wall(Cluj,2,2,Direction.EAST);
        Wall block6=new Wall(Cluj,1,2,Direction.EAST);
        Wall block7=new Wall(Cluj,1,1,Direction.NORTH);
        Wall block8=new Wall(Cluj,1,2,Direction.NORTH);
        Robot r1 = new Robot(Cluj, 0, 2, Direction.WEST);
        r1.move();
        r1.move();
        r1.turnLeft();
        r1.move();
        r1.move();
        r1.move();
        r1.turnLeft();
        r1.move();
        r1.move();
        r1.move();
        r1.turnLeft();
        r1.move();
        r1.move();
        r1.move();
        r1.turnLeft();
        r1.move();
    }
}
