package ex2;

public interface Image {
    String display();
}

class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public String display() {
        return "Displaying " + fileName;
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}

/*class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public String display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        return realImage.display();
    }
}*/
class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public String display() {
        if (fileName.endsWith("png"))
        {
            realImage = new RealImage(fileName);
            return realImage.display();
        }
        else {
            rotatedImage = new RotatedImage();
            return rotatedImage.display();
        }
    }
}
class RotatedImage implements Image {

    private String fileName;

    @Override
    public String display() {
        return "Display rotated " + fileName;
    }
}
