package ex1;

class Square extends Rectangle {
    private double side;
    public Square(){
    }
    public Square(double side){
        this.side=side;
    }
    public Square(double side, String color, boolean filled){
        super(side, side);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    public double getPerimeter() {
        return 4*side;
    }
    public double getArea() {
        return Math.pow(side,2);
    }
    @Override
    public String toString() {
        return "ex1.Circle{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                ", width=" + width +
                ", length=" + length +
                '}';
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }
}