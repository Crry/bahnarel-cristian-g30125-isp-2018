package ex1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAll {
    public static void main(String[] args){
        Circle c1 = new Circle(3);
        System.out.println("Perimetrul cercului e " + c1.getPerimeter());
        System.out.println("Aria cercului e " + c1.getArea());
        Rectangle r1 = new Rectangle(3,5);
        System.out.println("Perimetrul patrulaterului e " + r1.getPerimeter());
        System.out.println("Aria patrulaterului e " + r1.getArea());
        Square s1 = new Square(4);
        System.out.println("Perimetrul patratului e " + s1.getPerimeter());
        System.out.println("Aria patratului e " + s1.getArea());
        }
        @Test
        public void shouldreturncirclesperimeter(){
        Circle c2 = new Circle(3);
        assertEquals(18.849, c2.getPerimeter(),001);
    }
    @Test
    public void shouldreturncirclesarea(){
        Circle c3 = new Circle(3);
        assertEquals(28.274, c3.getArea(),001);
    }
    @Test
    public void shouldreturnrectanglesperimeter(){
        Rectangle r2 = new Rectangle(3,5);
        assertEquals(16, r2.getPerimeter(),1);
    }
    @Test
    public void shouldreturnrectanglesarea(){
        Rectangle r3 = new Rectangle(3,5);
        assertEquals(15, r3.getPerimeter(),1);
    }
    @Test
    public void shouldreturnsquaresperimeter(){
        Square s2 = new Square(5);
        assertEquals(20, s2.getPerimeter(),1);
    }
    @Test
    public void shouldreturnsquaresarea(){
        Square s3 = new Square(5);
        assertEquals(25, s3.getArea(),1);
    }
}
