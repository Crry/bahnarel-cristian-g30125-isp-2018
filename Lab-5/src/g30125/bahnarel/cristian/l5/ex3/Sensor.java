package ex3;

import java.util.Scanner;

public abstract class Sensor {

    String location;

    abstract int readValue();

    public String getLocation(){
        System.out.println("Introduceti locatia:");
        Scanner in = new Scanner(System.in);
        location = in.nextLine();
        return location;
    }
}
