package ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {

    public int readValue() {

        Random generator = new Random();
        return generator.nextInt(100);
    }
}
