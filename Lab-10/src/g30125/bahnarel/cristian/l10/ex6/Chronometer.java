package g30125.bahnarel.cristian.l10.ex6;

public class Chronometer implements Runnable{
    int k=0;
    Integer syncObj = 0;
    boolean p = true;

    Chronometer(){
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true){
            if(p){
                synchronized (syncObj){
                    try {
                        syncObj.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            } else {
                synchronized (this){
                    this.notify();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
            k++;
        }
    }

    public void changeState() {
        p = !p;
        if(p){
            synchronized (syncObj){
                syncObj.notify();
            }
        }
    }
    public static void main(String[] args) {
        Chronometer c = new Chronometer();
        UI ui = new UI();
        ChronometerController CC = new ChronometerController(c,ui);
    }
}
