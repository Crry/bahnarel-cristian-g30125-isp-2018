package g30125.bahnarel.cristian.l10.ex6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChronometerController implements ActionListener {
    Chronometer c;
    UI u;

    public ChronometerController(Chronometer c, UI u){
        this.c=c;
        this.u=u;
        u.getButton1().addActionListener(this);
        u.getButton2().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==u.getButton1()){
            c.changeState();
            u.getTextField().setText(c.k+"");
        } else if (e.getSource()==u.getButton2()){
            c.changeState();
            c.k=0;
        }
    }
}
