package g30125.bahnarel.cristian.l10.ex6;

import javax.swing.*;
import java.awt.*;

public class UI  extends JFrame {

    private JButton button1 = new JButton("Start/Stop");
    private JButton button2 = new JButton("Reset");
    private TextField textField=new TextField();

    public UI() throws HeadlessException {
        setSize(400,400);
        setLayout(new FlowLayout());
        textField.setPreferredSize(new Dimension(150,150));
        add(textField);
        add(button1);
        add(button2);
        setVisible(true);
    }
    public JButton getButton1() {
        return button1;
    }
    public JButton getButton2() {
        return button2;
    }
    public TextField getTextField() {
        return textField;
    }
}