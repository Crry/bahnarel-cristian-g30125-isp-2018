package ex4;

import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args) {

        Dictionary d = new Dictionary();
        int x = 1;
        String word;
        String definition;

        while (x!=0){
            displayMenu();
            Scanner in = new Scanner(System.in);
            x = in.nextInt();

            switch (x) {
                case 1: {
                    System.out.println("Introduceti cuvant: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    System.out.println("Introduceti definitie: ");
                    Scanner inDef = new Scanner(System.in);
                    definition = inDef.next();
                    d.addWord(new Word(word),new Definition(definition));
                    break;
                }
                case 2: {
                    System.out.println("Enter the word: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    d.getDefinition(new Word(word));
                    break;
                }
                case 3: {
                    d.getAllWords();
                    break;
                }
                case 4: {
                    d.getAllDefinitions();
                    break;
                }
                case 0: {
                    break;
                }
            }
        }
    }

    private static void displayMenu() {
        System.out.println("Meniu:");
        System.out.println("1. Adauga cuvant si definitie");
        System.out.println("2. Arata definitia unui cuvant");
        System.out.println("3. Printeaza toate cuvintele");
        System.out.println("4. Printeaza toate cuvintele si definitiile lor");
        System.out.println("0. Exit");
        System.out.println("Introduceti tasta: ");
    }
}