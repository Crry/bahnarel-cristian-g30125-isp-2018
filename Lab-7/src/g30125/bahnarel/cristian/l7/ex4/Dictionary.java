package ex4;

import java.util.*;

public class Dictionary {

    private HashMap <Word,Definition> dictionary = new HashMap<Word,Definition>();
    private ArrayList <Word> wordList = new ArrayList<Word>();
    private ArrayList <Word> wordList1 = new ArrayList<Word>();

    public Dictionary() {
    }

    public void addWord(Word w, Definition d){
        dictionary.put(w,d);
    }

    public void getDefinition(Word w){
        if (dictionary.containsKey(w))
            System.out.println(w.getName() + " = " + dictionary.get(w).getDescription());
        else
            System.out.println("The word cannot be found in the dictionary.");
    }

    public void getAllWords() {
        if (dictionary.isEmpty()) {
            System.out.println("There are no words in the dictionary.");
        } else {
            for (Word w : dictionary.keySet()) {
                wordList.add(w);
            }
            Collections.sort(wordList);
            for (Word w : wordList) {
                System.out.println(w.getName());
            }
        }
    }

    public void getAllDefinitions(){
        if (dictionary.isEmpty()){
            System.out.println("There are no words in the dictionary.");
        }
        else {
            for (Word w : dictionary.keySet()) {
                wordList1.add(w);
            }
            Collections.sort(wordList1);
            for (Word w : wordList1) {
                System.out.println(w.getName() + " = " + dictionary.get(w).getDescription());
            }
        }
    }
}
