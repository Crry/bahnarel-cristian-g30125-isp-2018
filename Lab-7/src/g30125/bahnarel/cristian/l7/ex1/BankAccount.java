package ex1;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {

        this.balance -= amount;
    }

    public void deposit(double amount) {

        this.balance += amount;
    }

    public String getOwner() {

        return this.owner;
    }

    public double getBalance() {

        return balance;
    }

    @Override
    public boolean equals(Object b) {
        if(b instanceof BankAccount){
            BankAccount bank = (BankAccount) b;
            return (owner == ((BankAccount) b).owner && balance == ((BankAccount) b).balance);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return owner.hashCode()+(int)balance;
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Crry", 250);
        BankAccount b2 = new BankAccount("Crry", 250);
        System.out.println("Prima testare:");
        if (b1.equals(b2)) {
            System.out.println("They are equal\n");
        } else {
            System.out.println("They are not equal\n");
        }
        BankAccount b3 = new BankAccount("Nume1\n", 300);
        BankAccount b4 = new BankAccount("Menu2\n", 600);
        System.out.println("A doua testare:");
        if (b3.equals(b4)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are not equal");
        }
    }
}
