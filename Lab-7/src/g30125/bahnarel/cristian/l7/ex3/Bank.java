package ex3;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Bank {
    TreeSet<BankAccount> Accounts = new TreeSet<>();

    public void addAccount(String owner, double balance) {
        Accounts.add(new BankAccount(owner, balance));
    }


    public void printAccounts() {
        Comparator<BankAccount> byBalance = Comparator.comparingDouble(BankAccount::getBalance);
        Supplier<TreeSet<BankAccount>> suplier = () -> new TreeSet<BankAccount>(byBalance);

        TreeSet<BankAccount> sort = new TreeSet<>();
        sort = Accounts.stream().collect(Collectors.toCollection(suplier));
        for (BankAccount b : sort)
            System.out.println(b.getBalance() + " " + b.getOwner());
    }


    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount b : Accounts)
            if (b.getBalance() > minBalance && b.getBalance() < maxBalance)
                System.out.println(b.getBalance() + " " + b.getOwner());
    }


    public TreeSet<BankAccount> getAllAcounts() {
        Comparator<BankAccount> byName = Comparator.comparing(BankAccount::getOwner);
        Supplier<TreeSet<BankAccount>> suplier = () -> new TreeSet<BankAccount>(byName);

        return Accounts.stream().collect(Collectors.toCollection(suplier));


    }
    public static void main(String[] args){
        Bank bank = new Bank();
        bank.addAccount("Crry",7500);
        bank.addAccount("Corny",3000);
        bank.addAccount("Sebi",400);
        bank.addAccount("Lau",1500);
        bank.addAccount("Andreea",8000);
        System.out.println("Sortarea dupa balance:");
        bank.printAccounts();
        System.out.println();
        System.out.println("Sortarea intre un minim si maxim:");
        bank.printAccounts(399,3000);

        System.out.println();
        System.out.println("Sortarea alfabetica:");
        for(BankAccount b : bank.getAllAcounts()){
            System.out.println(b.getOwner() + " " + b.getBalance());
        }
    }
}



