package ex2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Bank
{
    ArrayList<BankAccount> Accounts = new ArrayList<BankAccount>();
    public void addAccount(String owner, double balance)
    {
        Accounts.add(new BankAccount(owner,balance));
    }
    public void printAccounts()
    {
        Collections.sort(Accounts);
        for(BankAccount b:Accounts)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public void printAccounts(double minBalance, double maxBalance)
    {
        for(BankAccount b:Accounts)
            if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
                System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public BankAccount getAccount(String owner)
    {
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts() {
        return Accounts;
    }


    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Crry",7500);
        bank.addAccount("Corny",3000);
        bank.addAccount("Sebi",400);
        bank.addAccount("Lau",1500);
        bank.addAccount("Andreea",8000);
        System.out.println("Sortarea dupa balance:");
        bank.printAccounts();
        System.out.println();
        System.out.println("Sortarea intre un minim si maxim:");
        bank.printAccounts(399,3001);
        ArrayList<BankAccount> bankAccounts = bank.getAllAccounts();
        Collections.sort(bankAccounts,BankAccount.ownerComparator);
        System.out.println();
        System.out.println("Sortarea alfabetica:");
        for(BankAccount b:bankAccounts)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }
}
