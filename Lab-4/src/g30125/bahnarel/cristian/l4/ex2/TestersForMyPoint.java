package ex2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestersForMyPoint {
    @Test
    public void shouldReturndistance()
    {
        MyPoint p = new MyPoint(23,23);
        //int x=5;
        //int y=7;
        assertEquals(35, p.distance(-1,-2), 1);
    }
}