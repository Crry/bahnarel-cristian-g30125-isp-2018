package ex6;

import ex4.Author;

public class TestBook {
    public static void main(String[] args)
    {
        Author[] authors = new Author[3];
        authors[0] = new Author("Marcel", "marcel@yahoo.com", 'm');
        authors[1] = new Author("Marinela", "marinela@yahoo.com", 'f');
        authors[2] = new Author("Meme", "meme@yahoo.com", 'm');
        Book b1 = new Book("Ape Inbolburate", authors, 200, 20);
        System.out.println(b1.toString());
    }
}
