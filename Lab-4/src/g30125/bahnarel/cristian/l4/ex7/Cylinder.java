package ex7;

import ex3.Circle;

public class Cylinder extends Circle{
    private double height;

    public Cylinder(){
        this.height=1.0;
    }
    public Cylinder(double rad){
        getRadius();
    }
    public Cylinder(double rad, double height)
    {
        getRadius();
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getAreagresit() {

        return Math.PI*Math.pow(getRadius(),2);
    }
    public double getVolume(double radius){
        return 2*Math.PI*Math.pow(radius,2)*getHeight();
    }
    @Override
    public double getArea(double radius) {
        return 2*Math.PI*radius*(radius+getHeight());
    }

}
