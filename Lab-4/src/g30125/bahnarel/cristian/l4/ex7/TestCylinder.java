package ex7;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(2.0,5.0);
        System.out.println("Aria calculata gresit: " + c1.getAreagresit());
        System.out.println("Aria calculata corect: " + c1.getArea(2));
        System.out.println("Volumul calculat este: " + c1.getVolume(2));
    }
}
