package ex5;

import ex4.Author;

public class TestBook {
    public static void main(String[] args)
    {
        Book t1 = new Book("Ape Tulburi", new Author("Aurel", "aurel@yahoo.com", 'm') , 200);
        System.out.println(t1.toString());
        Book t2 = new Book("Munti si Vanturi", new Author("Georgica", "georgica@yahoo.com", 'm') , 200 , 10);
        System.out.println(t2.toString(300));
        Book t3 = new Book("Munti si Vanturi", new Author("Georgica", "georgica@yahoo.com", 'm') , 200 , 10);
        t3.setPrice(500);
        t3.setQtyInStock(14);
        System.out.println("Testing setters: ");
        System.out.println(t3.getPrice());
        System.out.println(t3.getQtyInStock());
    }
}
