package ex8;

public class Shape {
    private String color;
    private boolean filled;

    public Shape(){
        this.color="green";
        this.filled=true;
    }
    public Shape(String color, boolean filled){
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public boolean getFILLED(){
        return filled;
    }
    public void setFILLED(boolean filled){
        this.filled=filled;
    }

    @Override
    public String toString() {
        int k = 0;
        if (getFILLED()==true) k = 1;
        if (k == 1) {
            return "A shape with color of " + getColor() + " and filled.";
        }
        else return "A shape with color of " + getColor() + " and Not filled.";
    }
}
