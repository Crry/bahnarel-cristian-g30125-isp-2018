package ex8;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
public class TestersForEx8 {
    @Test
    public void shouldreturnCirclePerimeter(){
        Circle c6 = new Circle(2);
        assertEquals(12.56, c6.getPerimeter(2),001);
    }
    @Test
    public void shouldreturnCircleArea(){
        Circle c7 = new Circle(2);
        assertEquals(12.56, c7.getArea(2), 001);
    }
    @Test
    public void shouldreturnRectanglePerimeter(){
        Rectangle r6 = new Rectangle(3,2);
        assertEquals(10.0, r6.getPerimeter(),001);
    }
    @Test
    public void shouldreturnRectangleArea(){
        Rectangle r6 = new Rectangle(3,2);
        assertEquals(6.0, r6.getArea(),001);
    }
}
