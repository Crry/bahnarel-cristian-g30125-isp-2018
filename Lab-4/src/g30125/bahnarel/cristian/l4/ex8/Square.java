package ex8;

public class Square extends Rectangle{

    public Square(){
        super();
    }
    public Square(double side){
        super(side,side);
    }
    public Square(double side, String color, boolean filled){
        super(side,side);
    }
    @Override
    public String toString(){
        return "A Square with side "+ super.getWidth()+",which is a subclass of "+ super.toString();
    }
    public void setLength(double length){
        this.getLength();
    }
    public void setWidth(double width){
        this.getWidth();
    }
}