package ex9;

import becker.robots.*;

public class PoolDiving {
    public static void main(String[] args) {
        City ny = new City();
        Wall blockAve0 = new Wall(ny, 4, 3, Direction.EAST);
        Wall blockAve1 = new Wall(ny, 3, 3, Direction.EAST);
        Wall blockAve2 = new Wall(ny, 2, 3, Direction.EAST);
        Wall blockAve3 = new Wall(ny, 4, 4, Direction.WEST);
        Wall blockAve4 = new Wall(ny, 2, 4, Direction.NORTH);
        Wall blockAve5 = new Wall(ny, 4, 4, Direction.SOUTH);
        Wall blockAve6 = new Wall(ny, 4, 5, Direction.SOUTH);
        Wall blockAve7 = new Wall(ny, 4, 5, Direction.EAST);
        Robot mark = new Robot(ny, 1, 4, Direction.NORTH);
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
    }
}
