package ex4;

public class TestAuthor {
    public static void main(String[] args)
    {
        Author t1 = new Author("Aurel", "aurel@yahoo.com", 'm');
        System.out.println(t1.toString());
        t1.setEmail("blablabla@yahoo.com");
        System.out.println("Testing setter: " + t1.getEmail());
    }
}
