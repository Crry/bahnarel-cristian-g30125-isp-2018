package ex3;

public class TestCircle {
    public static void main(String[] args){
        Circle t1 = new Circle();
        System.out.println(t1.getArea(7));
        System.out.println(t1.getRadius());
        Circle t2 = new Circle("purple", 77);
        System.out.println(t2.getRadius());
        System.out.println(t2.getArea(7));
    }
}
