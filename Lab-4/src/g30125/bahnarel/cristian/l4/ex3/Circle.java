package ex3;


public class Circle {
    private String color="red";
    private double radius=1;

    public Circle()
    {
        this.color= color;
        this.radius=radius;
    }
    public Circle(String col, double rad)
    {
        this.color=col;
        this.radius=rad;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(double radius) {
        return Math.PI*Math.pow(radius,2);
    }
}
